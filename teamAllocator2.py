from os.path import exists
import os
'''
    This is the team allocator project solution example project
'''


def student_list():
    return ['zakithikhDBN2022 - 4 April - Johannesburg Physical - seat 3', 'ddhaalJHB2022 - 2 May - Cape Town Virtual',
    'thandohDBN2022 - 4 April - Phokeng Physical - seat 3', 'zaneleJHB2022 - 2 May - Durban Virtual',
    'ntobekoDBN2022 - 4 April - Phokeng Physical - seat 2', 'BusiJHB2022 - 2 May - Durban Virtual',
    'zinhlehDBN2022 - 4 April - Phokeng Physical - seat 1', 'CebiJHB2022 - 2 May - Durban Virtual',
    'lukhona - 4 April - Phokeng Virtual', 'ddhaalJHB2022 - 2 May - Durban Physical - seat 4',
    'gabiDBN2022 - 4 April - Phokeng Virtual', 'ngakithilJHB2022 - 2 May - Durban Physical - seat 5',
    'zimthembilehDBN2022 - 4 April - Phokeng Virtual', 'ngakuyelJHB2022 - 2 May - Durban Physical - seat 2',
    'zimlindilehDBN2022 - 4 April - Phokeng Virtual', 'yenzileJHB2022 - 2 May - Durban Physical - seat 3',
    'zimthandilehDBN2022 - 4 April - Johannesburg Virtual','kuhlengaweDBN2022 - 4 April - Durban Physical - seat 1',
    'zimkhonzileDBN2022 - 4 April - Johannesburg Virtual','hlelokuhlehDBN2022 - 4 April - Durban Physical - seat 3',
    'zizonkehDBN2022 - 4 April - Johannesburg Virtual','sibusisohDBN2022 - 4 April - Durban Physical - seat 2',
    'kholekileDBN2022 - 4 April - Johannesburg Virtual','vusiDBN2022 - 4 April - Durban Physical - seat 9',
    'kholelwahDBN2022 - 4 April - Johannesburg Virtual','zuzumuzihDBN2022 - 4 April - Durban Physical - seat 10',
    'thembelahDBN2022 - 4 April - Johannesburg Virtual','babazileDBN2022 - 4 April - Durban Physical - seat 11',
    'thembisileDBN2022 - 4 April - Johannesburg Virtual','owenkosiDBN2022 - 4 April - Durban Physical - seat 8',
    'thembisiweDBN2022 - 4 April - Johannesburg Physical - seat 5', 'nobuhleJHB2022 - 2 May - Cape Town physical',
    'thenjisiweDBN2022 - 4 April - Johannesburg Physical - seat 6', 'nonkonzoJHB2022 - 2 May - Cape Town Physical',
    'thethelelileDBN2022 - 4 April - Johannesburg Physical - seat 7', 'nombusoJHB2022 - 2 May - Cape Town Virtual',
    'thembiDBN2022 - 4 April - Johannesburg Physical - seat 4', 'nozizweJHB2022 - 2 May - Cape Town Virtual']


def dbn_campus_students(student_list):
    '''
    from the list of students above, fill in this function to return a list of all
    students in the Durban campus only.
    '''

    # declare a new list
    dbn_students = []

    # access the student_list by using a for loop
    for i in student_list:
        if 'Durban' in i:
            i = i.replace(" ", "").lower()
            dbn_students.append(i)
    
    # print the values inside the dbn_students list
    print(dbn_students)

    return dbn_students


def cpt_campus_students(student_list):
    '''
    from the list of students above, fill in this function to return a list of all
    students in the Cape Town campus only.
    '''
    cpt_students = []

    # access the student_list by using a for loop
    for i in student_list:
        if 'Cape Town' in i:
            i = i.replace(" ", "").lower()
            cpt_students.append(i)
    
    # print the values inside the cpt_students list
    print(cpt_students)

    return cpt_students


def jhb_campus_students(student_list):
    '''
    from the list of students above, fill in this function to return a list of all
    students in the Johannesburg campus only.
    '''
    # create a new list
    jhb_students = []

    # access the student list
    for i in student_list:
        if 'Johannesburg' in i:
            i = i.replace(" ", "").lower()
            jhb_students.append(i)
    
    # print the values inside the jhb_students list
    print(jhb_students)

    return jhb_students


def nw_campus_students(student_list):
    '''
    from the list of students above, fill in this function to return a list of all
    students in the North West campus only.
    '''
    # create a nw_students list
    nw_students = []

    for i in student_list:
        if 'Phokeng' in i:
            i = i.replace(" ", "").lower()
            nw_students.append(i)
    
    # print the values inside the nw_students list
    print(nw_students)

    return nw_students

    
def dbn_physical_students(dbn_students):
    '''
    from the list of dbn_campus_students, fill in this function to return a list of all
    students who will be attending physically on campus
    '''
    # create a new list to store the students name who are attending physically
    dbn_physical_list = []

    # access the dbn_students list to get values
    for i in dbn_students:
        if 'Durban Physical' in i:
            i = i.replace(" ", "").lower()
            dbn_physical_list.append(i)

    # print the new list    
    print(dbn_physical_list)

    return dbn_physical_list

def dbn_physical_teams(dbn_physical_students):
    '''
    from the list of dbn_physical_students create list of 4 students per team, and add them to 
    one big list
    '''
    # call the dbn_physical function
    dbn_physical_students(list)

    # create a new list 
    dbn_physical_teams_list = []

    # this will group each  4 elements in the order they appear
    # if "Durban" in dbn_physical_students:
    dbn_physical_teams_list = [dbn_physical_students[i:i+4] for i in range(0, len(dbn_physical_students), 4)]

    print(dbn_physical_teams_list)
    # return the list    
    return dbn_physical_teams_list

def dbn_teams_file(durban_physical_teams):
    '''
    write and save the information in the dbn_physical_teams into a textfile
    '''
    

def cpt_physical_students(cpt_physical_students):
    '''
    from the list of cpt_campus_students, fill in this function to return a list of all
    students who will be attending physically on campus
    '''
    cpt_physical_list = []

    for i in cpt_physical_students:
        if 'Cape Town Physical' in i:
            i = i.replace(" ", "").lower()
            cpt_physical_list.append(i)

    # print the new list    
    print(cpt_physical_list)

    return cpt_physical_list

def cpt_physical_teams(cpt_physical_teams):
    '''
    from the list of cpt_physical_students create list of 4 students per team, and add them to 
    one big list
    '''
    
    return cpt_physical_teams

def cpt_teams_file(capetown_final_teams):
    '''
    write and save the information in the cpt_physical_teams into a textfile
    '''


def jhb_physical_students(jhb_physical_students):
    '''
    from the list of jhb_campus_students, fill in this function to return a list of all
    students who will be attending physically on campus
    '''
    jhb_physical_list = []

    for i in jhb_physical_students:
        if 'Johannesburg Physical' in i:
            i = i.replace(" ", "").lower()
            jhb_physical_list.append(i)

    # print the new list    
    print(jhb_physical_list)

    return jhb_physical_list


def jhb_physical_teams(jhb_physical_teams):
    '''
    from the list of jhb_physical_students create list of 4 students per team, and add them to 
    one big list
    '''
    jhb_physical_teams_list = []

    for i in jhb_physical_teams:
        if ""

    jhb_physical_teams_list = [jhb_physical_students[i:i+4] for i in range(0, len(dbn_physical_students), 4)]

    return jhb_physical_teams_list

def jhb_teams_file(jhb_final_teams):
    '''
    write and save the information in the jhb_physical_teams into a textfile
    '''


def nw_physical_students(nw_physical_students):
    '''
    from the list of nw_campus_students, fill in this function to return a list of all
    students who will be attending physically on campus
    '''
    nw_physical_list = []

    for i in nw_physical_students:
        if 'Phokeng Physical' in i:
            i = i.replace(" ", "").lower()
            nw_physical_list.append(i)

      
    print(nw_physical_list)
    
    return nw_physical_list


def nw_physical_teams(nw_physical_teams):
    '''
    from the list of nw_physical_students, create list of 4 students per team, and add them to 
    one big list
    '''


    return nw_physical_teams


def nw_teams_file(nw_final_teams):
    '''
    write and save the information in the nw_physical_teams into a textfile
    '''


def get_virtual_students(student_list):
    '''
    from the list of students above, fill in this function to return a list of all
    students attending virtually.
    '''
    virtual_campus = []

    for i in student_list:
        if 'Virtual' in i:
            # i = i.replace(" ", "").lower()
            virtual_campus.append(i)
    
    # print the values inside the virtual_campus list
    print(virtual_campus)

    return virtual_campus


def virtual_teams(virtual_students_list):
    '''
    from the list of virtual_students above,  create list of 4 students per team, and add them to 
        one big list
    '''
    virtual_teams = []

    return virtual_teams


def virtual_teams_file(virtual_teams):
    '''
    write and save the information in the virtual_teams into a textfile
    '''
    get_virtual_students(student_list)
    # create a textfile
    f = open("virtual_teams.txt", 'w+')

    # enter data on the file
    for i in get_virtual_students:
        f.write(i)

    # close the file
    f.close()

    # check if the file created exists

    file_path = ("./virtual_teams.txt")
    flag = os.path.isfile(file_path)

    if flag:
        return True
    else:
        return False
    # return f
    

if __name__ == '__main__':
    '''
    call all your functions below to make your program execute    
    '''
    pass
    # assing the list to a new variable
    list = student_list()

    # call the dbn_campus_students function
    dbn_campus_students(list)
    
    print("")
    # call the cpt_campus_students function
    cpt_campus_students(list)
    
    print("")
    # call the jhb_campus_students function
    jhb_campus_students(list)
    
    print("")
    # call the nw_campus_students function
    nw_campus_students(list)

    print("")
    # call the durban_physical_students function
    dbn_physical_students(list)
    
    print("")
    print("the grouping of items")
    #dbn_physical_teams(list)

    cpt_physical_students(list)

    jhb_physical_students(list)

    nw_physical_students(list)

    print("print virtual ")
    get_virtual_students(list)

    virtual_teams_file(list)
